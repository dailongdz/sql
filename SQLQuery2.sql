create database TH1808037_T1809E_MXH;
use TH1808037_T1809E_MXH;

create table users_
(
account_ nvarchar primary key,
email_ nvarchar(50),
password_ nvarchar,
avartar_ image,
status_ nvarchar,
join_date date,
birth_date date,
);

create table post_
(
post_id nvarchar(20) primary key,
post_content nvarchar(200),
account_ nvarchar,
foreign key (account_) references users_(account_),
);

create table comment_
(
comment_id nvarchar(100) primary key,
comment_content nvarchar(200),
comment_time date,
account_ nvarchar,
post_id nvarchar(20),
foreign key (account_) references users_(account_),
foreign key (post_id) references post_(post_id),
);

create table action_
(
type_ nvarchar,
action_id nvarchar primary key,
action_time nvarchar,
account_ nvarchar,
post_id nvarchar(20), 
foreign key (account_) references users_(account_),
foreign key (post_id) references post_(post_id),
);

create table group_
(
group_name nvarchar,
group_id nvarchar primary key,
group_type nvarchar,
);

create table contact_
(
admin_ nvarchar,
officer_ nvarchar,
account_ nvarchar,
group_id nvarchar, 
foreign key (account_) references users_(account_),
foreign key (group_id) references group_(group_id),
);